﻿using UnityEngine;

public class PlayMusic : MonoBehaviour {
    AudioSource music;
    public AudioClip[] clip;
    public float fadeRate;
    private bool caosMusic = false, peaceMusic = false;
    private bool fadeOut = false, fadeIn = false;

    void OnTriggerEnter(Collider co)
    {
        // "caos" y "peace" son los nombres de los objetos que tienen los colliders que cambian la música
        if (co.name == "caos" && music.clip != clip[0])
        {
            caosMusic = true;
            fadeOut = true;
        }
        if (co.name == "peace" && music.clip !=clip[1])
        {
            peaceMusic = true;
            fadeOut = true;
        }

        Debug.Log("OnTigger");
    }

    void Start()
    {
        music = GetComponent<AudioSource>();
        music.clip = clip[0];
        music.Play();
    }

    void Update () {
        if (fadeIn){
            if (caosMusic)
            {
                music.clip = clip[0];
                caosMusic = false;
                music.Play();
            }
            else if (peaceMusic)
            {
                music.clip = clip[1];
                peaceMusic = false;
                music.Play();
            }

        }
        if (music.volume > 0.1f && fadeOut && !fadeIn)
            music.volume -= fadeRate * Time.deltaTime;
        else if (fadeOut){
            music.volume = 0.0f;
            music.Play();
            fadeOut = false;
            fadeIn = true;
        }
        if (music.volume < 0.9f && fadeIn && !fadeOut) {
            music.volume += fadeRate * Time.deltaTime;
        }
        else if (fadeIn)
        {
            music.volume = 1.0f;
            fadeIn = false;
        }
    }
}
