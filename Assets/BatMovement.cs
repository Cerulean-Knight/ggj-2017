﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatMovement: MonoBehaviour {

    public float maxSpeed, speed;
    private Vector3 movimiento = Vector3.forward;

    void Update () {
        GetComponent<Rigidbody>().velocity = movimiento * speed;
        if (Input.GetKeyDown(KeyCode.Space)){
            speed -= 4;
        }
        if (Input.GetKeyDown(KeyCode.W)) {
            movimiento = Vector3.forward;
        }
        if (Input.GetKeyDown(KeyCode.S)){
            movimiento = Vector3.back;
        }
        if (Input.GetKeyDown(KeyCode.A)){
            movimiento = Vector3.left;
        }
        if (Input.GetKeyDown(KeyCode.D)){
            movimiento = Vector3.right;
        }
        if (speed < maxSpeed){
            speed += Time.deltaTime * 2;
        }
    }
}
