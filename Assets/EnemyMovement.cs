﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour {

    public float speed;
    public BatMovement objetivo;
    private Vector3 objetivoDirection;

    void OnTriggerEnter(Collider co){
        if (co.name == objetivo.GetComponent<Collider>().name)
            Debug.Log("Dead"); 
    }

    void Update() {
        objetivoDirection = objetivo.transform.position - transform.position;
        GetComponent<Rigidbody>().velocity = objetivoDirection.normalized * speed;
    }
}
